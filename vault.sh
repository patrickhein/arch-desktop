#!/bin/bash

ansible-vault --vault-password-file ~/.server-cluster-vault-pass "$@"
